from flask import Flask, jsonify, request
from flask_restful import Resource, Api
from flask_cors import CORS
from nltk.corpus import stopwords 
from nltk.tokenize import word_tokenize, sent_tokenize 
from nltk.sentiment.vader import SentimentIntensityAnalyzer

app = Flask(__name__)
api = Api(app)
CORS(app)


class review (Resource):
    
    def text_summarization(self,text):
        stopWords = set(stopwords.words("english")) 
        words = word_tokenize(text) 
        freqTable = dict() 
        for word in words: 
            word = word.lower()
            if word in stopWords: 
                continue
            if word in freqTable: 
                freqTable[word] += 1
            else: 
                freqTable[word] = 1
        sentences = sent_tokenize(text) 
        sentenceValue = dict() 
        for sentence in sentences: 
            for word, freq in freqTable.items(): 
                if word in sentence.lower(): 
                    if sentence in sentenceValue: 
                        sentenceValue[sentence] += freq 
                    else: 
                        sentenceValue[sentence] = freq 
        sumValues = 0
        for sentence in sentenceValue: 
            sumValues += sentenceValue[sentence] 
        average = int(sumValues / len(sentenceValue)) 
        summary = '' 
        for sentence in sentences: 
            if (sentence in sentenceValue) and (sentenceValue[sentence] > (1.2 * average)): 
                summary += " " + sentence 
        return summary 
    
    def sentiment_predict(self,text):
        sentiment=SentimentIntensityAnalyzer()
        score = sentiment.polarity_scores(text)
        value=score['compound']
        if value>=0.06999999999999937:
            return {"review":'It was good!!',"type":"Positive"}
        elif value> 0.05999999999999998 and value<0.06999999999999937:
            return {"review":'Good but can be better!',"type":"Neutral"}
        else:
            return {"review":'Need to improve',"type":"Negative"}
   

    def get(self):
        try:
            return {'data': 'Api is Running'}
        except:
            return {'data': 'An Error Occurred during fetching Api'}

    def post(self):
        try:
            json_data = request.get_json(force=True)
            data= json_data['review']
            review_predict = self.sentiment_predict(data)
            review_summary = self.text_summarization(data)
            return {'review_predict':review_predict,'review_summary':review_summary}
        except:
            return {'error': 'Unexpected Error'}

api.add_resource(review, '/review')

if __name__ == '__main__':
    app.run()